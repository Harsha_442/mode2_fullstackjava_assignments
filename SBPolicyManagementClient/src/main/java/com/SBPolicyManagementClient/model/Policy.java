package com.SBPolicyManagementClient.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="POLICY_DETAILS")
public class Policy {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="policy_id")
	private Long id;
	@Column(name="policy_name")
	private String policyName;
	@Column(name="policy_type")
	private String policyType;
	@Column(name="policy_duration")
	private String duration;
	@Column(name="policy_amount")
	private Integer amount;
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="userId")  //FK
	@JsonBackReference
	private UserPolicy userPolicy;
	public Policy() {
		super();
	}
	public Policy(Long id, String policyName, String policyType, String duration, Integer amount) {
		super();
		this.id = id;
		this.policyName = policyName;
		this.policyType = policyType;
		this.duration = duration;
		this.amount = amount;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPolicyName() {
		return policyName;
	}
	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}
	public String getPolicyType() {
		return policyType;
	}
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Policy(String policyName, String policyType, String duration, Integer amount) {
		super();
		this.policyName = policyName;
		this.policyType = policyType;
		this.duration = duration;
		this.amount = amount;
	}
	public UserPolicy getUserPolicy() {
		return userPolicy;
	}
	public void setUserPolicy(UserPolicy userPolicy) {
		this.userPolicy = userPolicy;
	}
	public Policy(Long id, String policyName, String policyType, String duration, Integer amount,
			UserPolicy userPolicy) {
		super();
		this.id = id;
		this.policyName = policyName;
		this.policyType = policyType;
		this.duration = duration;
		this.amount = amount;
		this.userPolicy = userPolicy;
	}
	
}