package com.SBPolicyManagementClient.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.SBPolicyManagementClient.model.UserPolicy;
import com.SBPolicyManagementClient.dao.UserPolicyDaoIntf;

@Service
@Transactional
public class UserPolicyServiceImpl implements UserPolicyServiceIntf {

	@Autowired
	UserPolicyDaoIntf userPolicyDaoIntf;
	public UserPolicy save(UserPolicy userPolicy) {
		
		return userPolicyDaoIntf.save(userPolicy);
	}
	
	public UserPolicy getUserByUserIdAndPassword(Integer id, String password) {
		
		return userPolicyDaoIntf.getUserByUserIdAndPassword(id,password);
	}

	@Override
	public UserPolicy findUserByUserId(Integer tempId) {
		
		return userPolicyDaoIntf.findUserByUserId(tempId);
	}

}
