package com.SBPolicyManagementClient.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.SBPolicyManagementClient.model.Policy;
import com.SBPolicyManagementClient.dao.PolicyDaoIntf;

@Service
@Transactional
public class PolicyServiceImpl implements PolicyServiceIntf {

	@Autowired
	PolicyDaoIntf repository;
	@Override
	public Policy save(Policy policy) {
		
		System.out.println(policy);
		return repository.save(policy);
	}

	@Override
	public List<Policy> findAll() {
		
		return repository.findAll();
	}

	public Policy findById(Long id) {
		
		Policy policy=null;
		Optional<Policy> optional = repository.findById(id);
		if (optional.isPresent()) {
			policy = optional.get();
		} else {
			System.err.println("No such employee");
		}
		return policy;
	}
		
	public Long deleteById(Long id) {
		
		repository.deleteById(id);
		return id;
	}

	@Override
	public List<Policy> findByPolicyName(String policyName) {
		
		return repository.findByPolicyName(policyName);
	}

}
