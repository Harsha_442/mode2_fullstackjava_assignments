package com.SBPolicyManagementClient.service;

import java.util.List;

import com.SBPolicyManagementClient.model.Policy;

public interface PolicyServiceIntf {
	
	public abstract Policy save(Policy policy);
	
	public abstract List<Policy> findAll();
	
	public abstract Policy findById(Long id);
	
	public abstract Long deleteById(Long id);

	public abstract List<Policy> findByPolicyName(String policyName);

}
