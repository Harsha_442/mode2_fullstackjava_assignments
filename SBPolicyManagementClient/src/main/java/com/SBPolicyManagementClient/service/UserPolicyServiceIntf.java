package com.SBPolicyManagementClient.service;

import com.SBPolicyManagementClient.model.UserPolicy;

public interface UserPolicyServiceIntf {
	
	public abstract UserPolicy save(UserPolicy userPolicy);

	public abstract UserPolicy getUserByUserIdAndPassword(Integer id, String password);

	public abstract UserPolicy findUserByUserId(Integer tempId);

}
