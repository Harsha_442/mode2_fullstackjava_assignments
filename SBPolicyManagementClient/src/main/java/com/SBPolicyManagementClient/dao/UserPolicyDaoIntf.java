package com.SBPolicyManagementClient.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.SBPolicyManagementClient.model.UserPolicy;

public interface UserPolicyDaoIntf extends JpaRepository<UserPolicy,Integer>{

	UserPolicy getUserByUserIdAndPassword(Integer id, String password);

	UserPolicy findUserByUserId(Integer tempId);

}
