package com.SBPolicyManagementClient.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.SBPolicyManagementClient.model.Policy;

public interface PolicyDaoIntf extends JpaRepository<Policy,Long>{

	List<Policy> findByPolicyName(String policyName);

}
