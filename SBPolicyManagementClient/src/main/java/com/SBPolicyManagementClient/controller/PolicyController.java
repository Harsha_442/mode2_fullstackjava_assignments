package com.SBPolicyManagementClient.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.SBPolicyManagementClient.model.Policy;
import com.SBPolicyManagementClient.service.PolicyServiceIntf;
import com.SBPolicyManagementClient.dao.PolicyDaoIntf;

@CrossOrigin("*")
@RestController
@RequestMapping("/api")
public class PolicyController {
	private Logger log = LoggerFactory.getLogger(PolicyController.class);

	@Autowired
	private PolicyServiceIntf policyServiceIntf;

	@Autowired
	PolicyDaoIntf repository;

	
	@PostMapping("/policies")
	public ResponseEntity<Policy> postPolicy(@RequestBody Policy policy) {
		try {
			log.info("Inside Post Policy");
			Policy _policy = policyServiceIntf.save(new Policy(policy.getPolicyName(), policy.getPolicyType(),
					policy.getDuration(), policy.getAmount()));
			return new ResponseEntity<>(_policy, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@GetMapping("/policies")
	public ResponseEntity<List<Policy>> getAllPolicies() {
		List<Policy> policies = new ArrayList<Policy>();
		try {
			policyServiceIntf.findAll().forEach(policies::add);

			if (policies.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(policies, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/policies/{id}")
	public ResponseEntity<Policy> getPolicyById(@PathVariable("id") long id) {
		Optional<Policy> policyData = repository.findById(id);

		if (policyData.isPresent()) {
			return new ResponseEntity<>(policyData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping(value = "/policies/update")
	public Policy updatePolicy(@RequestBody Policy policy) {
		log.info("Into update");
		log.info("into update" + policy.getId() + " " + policy.getPolicyName());
		return policyServiceIntf.save(policy);
				
		
	}

	@DeleteMapping(value = "/policies/{id}")
	@ResponseBody
	public ResponseEntity<String> deletePolicy(@PathVariable("id") long id) {
		log.info("deletePolicy");
		try {
			policyServiceIntf.deleteById(id);
			return new ResponseEntity<>("Deleted Successfully", HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>("Failed", HttpStatus.EXPECTATION_FAILED);
		}
	}

	@GetMapping(value = "policies/policyName/{policyName}")
	public ResponseEntity<List<Policy>> findByPolicyName(@PathVariable String policyName) {
		try {
			List<Policy> policies = policyServiceIntf.findByPolicyName(policyName);

			if (policies.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(policies, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	
}
