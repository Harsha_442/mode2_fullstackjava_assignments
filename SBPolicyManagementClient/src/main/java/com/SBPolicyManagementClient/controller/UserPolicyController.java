package com.SBPolicyManagementClient.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.SBPolicyManagementClient.model.UserPolicy;
import com.SBPolicyManagementClient.service.UserPolicyServiceIntf;

@CrossOrigin("*")
@RestController
@RequestMapping("/api")
public class UserPolicyController {
	
	private Logger log = LoggerFactory.getLogger(PolicyController.class);

	@Autowired
	UserPolicyServiceIntf userPolicyServiceIntf;
	
	@PostMapping("/user")
	public ResponseEntity<UserPolicy> postUserPolicy(@RequestBody UserPolicy userPolicy) throws Exception {
		Integer tempId=userPolicy.getUserId();
		if(tempId !=null)
		{
			UserPolicy userObj=userPolicyServiceIntf.findUserByUserId(tempId);
			if(userObj!=null) {
				throw new Exception("User with Id:"+tempId+" is already exists");
			}
		}
		
		try {
			log.info("Inside Post Policy");
			UserPolicy _userpolicy = userPolicyServiceIntf
					.save(new UserPolicy(userPolicy.getUserId(),userPolicy.getPassword(), userPolicy.getLastName(), userPolicy.getFirstName(),
							userPolicy.getAge(), userPolicy.getGender(), userPolicy.getPhone()));
			return new ResponseEntity<>(_userpolicy,HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@PostMapping("/loginuser")
	public UserPolicy loginUser(@RequestBody UserPolicy userPolicy) throws Exception {
		
		Integer id=userPolicy.getUserId();
		String  password=userPolicy.getPassword();
		UserPolicy userObj=null;
		if(id!=null && password!=null) {
			userObj=userPolicyServiceIntf.getUserByUserIdAndPassword(id,password);
		if(userObj==null)
		{
			throw new Exception("Bad Credentials");
		}
		}
		return userObj;
	}

}
