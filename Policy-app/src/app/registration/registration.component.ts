import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { RegistrationserviceService } from '../registrationservice.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  user:User=new User();
  submitted=false;
  msg="";
  message="";
  constructor(private registerService:RegistrationserviceService, private router:Router) {

   }

  ngOnInit() {
  }

  onSubmit()
  {
    console.log("Successfully Submitted");
    this.save();

  }
  save() {
    console.log(this.user);
    this.registerService.createUser(this.user)
    .subscribe(
      data => {
        console.log(data);
        this.message="Registration Successful";
        this.router.navigate(['login']);
        sessionStorage.setItem("key",this.message);
        this.submitted = true;
      },
      error =>{
      console.log(error);
      this.msg=error.error;
      this.user=new User();
      }
      )
  }

  newUser():void
  {
this.submitted=false;
this.user=new User();
  }

  goToLogin()
  {
    this.router.navigate(['login']);
  }
}
