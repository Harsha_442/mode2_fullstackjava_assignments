import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RegistrationserviceService {

  constructor(private http:HttpClient) { }

  private baseUrl = 'http://localhost:8082/api/user';

  createUser(user:any): Observable<any>{

    console.log(user);

    console.log(user);
    return this.http.post(this.baseUrl,user);
  }
}
