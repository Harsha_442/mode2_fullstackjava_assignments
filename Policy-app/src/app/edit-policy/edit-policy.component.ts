import { Component, OnInit } from '@angular/core';
import { PolicyServiceService } from '../policy-service.service';
import { Router } from '@angular/router';
import { Policy } from '../policy';

@Component({
  selector: 'app-edit-policy',
  templateUrl: './edit-policy.component.html',
  styleUrls: ['./edit-policy.component.css']
})
export class EditPolicyComponent implements OnInit {

  policy:Policy =new Policy();
  constructor(private policyservice:PolicyServiceService,private router:Router) { }

  ngOnInit() {

    this.editPolicy();
  }

  editPolicy()
  {
    let id=localStorage.getItem("id");
    this.policyservice.getPolicy(+id) //converts string to number
    .subscribe(data =>{
      this.policy=data;
    })
  }

  onUpdate()
  {
    console.log("into update");
    this.policyservice.updatePolicy(this.policy)
    .subscribe(data =>{
      console.log(data);
      this.router.navigate(["policy"]);
    }, error => console.log(error));
    this.policy=new Policy();
  }
}
