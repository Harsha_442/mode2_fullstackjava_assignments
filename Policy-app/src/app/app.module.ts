import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AddPolicyComponent } from './add-policy/add-policy.component';
import {HttpClientModule} from '@angular/common/http';
import { PolicyListComponent } from './policy-list/policy-list.component';
import { EditPolicyComponent } from './edit-policy/edit-policy.component';
import { SearchPolicyComponent } from './search-policy/search-policy.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component'
import { ConfirmEqualValidatorDirective } from './registration/confirm-equal-validator';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AddPolicyComponent,
    PolicyListComponent,
    EditPolicyComponent,
    SearchPolicyComponent,
    RegistrationComponent,
    LoginComponent,
    LogoutComponent,
    ConfirmEqualValidatorDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
