import { Injectable } from '@angular/core';
import { Observable,from } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PolicyServiceService {

  constructor(private http:HttpClient) { }

  private baseUrl = 'http://localhost:8082/api/policies';

  createPolicy(policy:any): Observable<any>
  {
    return this.http.post(this.baseUrl,policy);
  }

  getPolicies():Observable<any>{
    return this.http.get(this.baseUrl);
  }

  getPolicy(id:number):Observable<any>{
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  updatePolicy(policy: Object): Observable<Object>{
    console.log(this.baseUrl​​​​​​​​+"/update"​​​);
    console.log(policy);
    return this.http.put(`${this.baseUrl}` + `/update`, policy);
    
  }

  deletePolicy(id:number): Observable<any>{
    console.log(id);
    console.log(this.baseUrl​​​​​​​​+"/"+​​​​​id);
    
   const urlstr=this.baseUrl​​​​​​​​+"/"+​​​​​id;
   const options = {
    headers: new HttpHeaders({
      'Content-Type': 'text/html',
    }),
    };
    
   return this.http
    .delete(urlstr);
    
  }

  getPolicyByName(policyName:string): Observable<any>{
    return this.http.get(`${this.baseUrl}/policyName/${policyName}`);
  }
}
// return from( // wrap the fetch in a from if you need an rxjs Observable
    //   fetch(
    //   this.baseUrl + "/delete/" + id,
    //   {
    //  // body: JSON.stringify(data)
    //   headers: {
    //   'Content-Type': 'application/json',
    //   },
    //   method: 'GET',
    //   mode: 'no-cors'
    //   }
    //   )
    //   );
   // $http.delete({url:request,headers:{'Content-Type':'application/json'}})
   // .subscribe((s) => {
    // console.log(s);
    // });
    //  return this.http.delete({url:urlstr,headers:{'Content-Type':'application/json'}}).map(
    //   (res: Response) => {return res;}
    // )
  //;