import { Component, OnInit } from '@angular/core';
import { Policy } from '../policy';
import { PolicyServiceService } from '../policy-service.service';

@Component({
  selector: 'app-add-policy',
  templateUrl: './add-policy.component.html',
  styleUrls: ['./add-policy.component.css']
})
export class AddPolicyComponent implements OnInit {

  policy:Policy=new Policy();
  submitted=false;
   id=sessionStorage.getItem("uname");
  constructor(private policyService:PolicyServiceService) { }

  ngOnInit() {
  }

  onSubmit()
  {
    console.log("Successfully submitted");
    this.save();
  }

  save()
  {
    this.policyService.createPolicy(this.policy)
    .subscribe(
      data =>{
        console.log(data);
        this.submitted=true;
      },
      error => console.log(error));
      this.policy=new Policy();
  }
  newPolicy()
  {
    this.submitted=false;
    this.policy=new Policy();
  }
}
