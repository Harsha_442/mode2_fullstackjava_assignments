import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPolicyComponent } from './add-policy/add-policy.component';
import { PolicyListComponent } from './policy-list/policy-list.component';
import { EditPolicyComponent } from './edit-policy/edit-policy.component';
import { SearchPolicyComponent } from './search-policy/search-policy.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { AuthguardService } from './authguard.service';
import { LogoutComponent } from './logout/logout.component';


const routes: Routes = [
  {
    path:'add',
    component:AddPolicyComponent
  },
  {
    path:'policy',
    component:PolicyListComponent,
    canActivate:[AuthguardService]
  },
  {
    path:'edit',
    component:EditPolicyComponent
  },
  {
    path:'search',
    component:SearchPolicyComponent
  },
  {
    path:'register',
    component:RegistrationComponent
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'',
    redirectTo:'policy',
    pathMatch:'full'
  },
  {
    path:'logout',
    component:LogoutComponent,
    canActivate:[AuthguardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
