export class User {
    userId:number;
    firstName:string;
    lastName:string;
    age:number;
    gender:string;
    password:string;
    cpassword:string;
    phone:number;
}
