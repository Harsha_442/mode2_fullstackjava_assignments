import { Component, OnInit } from '@angular/core';
import { PolicyServiceService } from '../policy-service.service';
import { Policy } from '../policy';

@Component({
  selector: 'app-search-policy',
  templateUrl: './search-policy.component.html',
  styleUrls: ['./search-policy.component.css']
})
export class SearchPolicyComponent implements OnInit {

  policies:Policy[];
  constructor(private policyService:PolicyServiceService) { }

  ngOnInit() {
  }

  policyName:string;

  onSubmit()
  {
    
    this.searchPolicies();
  }

  submitted=false;
  searchPolicies()
  {

    this.policies=[];
    this.policyService.getPolicyByName(this.policyName)
    .subscribe(
      data => {
        this.policies=data;
        this.submitted=true;
      },
    );
  }
}
