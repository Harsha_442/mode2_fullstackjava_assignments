import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Policy } from '../policy';
import { PolicyServiceService } from '../policy-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-policy-list',
  templateUrl: './policy-list.component.html',
  styleUrls: ['./policy-list.component.css']
})
export class PolicyListComponent implements OnInit {

  policies:Observable<Policy[]>
  constructor(private policyService:PolicyServiceService, private router:Router) { }

  ngOnInit() {

    this.reloadData();
  }

  reloadData()
  {
    this.policies=this.policyService.getPolicies();
  }

  editPolicy(policy:Policy)
  {
    console.log("into edit");
    localStorage.setItem("id",policy.id.toString());
    this.router.navigate(["edit"]);
  }

  deletePolicy(policy:Policy)
  {
    console.log(policy.id);
    this.policyService.deletePolicy(policy.id)
    .subscribe(
      // data =>{
      //   console.log(data);
      //   this.reloadData();
      // },
      () =>{this.reloadData()},
      error => console.log(error));
  }
}
