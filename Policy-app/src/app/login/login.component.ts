import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';
import { User } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  //  username='';
  //  password='';
  // invalidLogin=false;


  registration=sessionStorage.getItem("key");
  user:User=new User();
  message='';
  constructor(private loginservice:AuthenticationService, private router:Router) { }

  ngOnInit() {
  }

  // checkLogin() {
  //   if (this.loginservice.authenticate(this.username, this.password)) {
  //     this.router.navigate([''])
  //     this.invalidLogin = false
  //   } else
  //     this.invalidLogin = true
  // }
   loginUser()
   {
     var a = this.user
    this.loginservice.loginUser(this.user)
     .subscribe(
       data => {
         console.log(data);
         this.save(data)
         sessionStorage.removeItem("key")
         this.router.navigate(['policy']);
      },
      error => {
        console.log("exception occured");
       this.message="Bad credentials, please enter valid userid and password";
     });

       }

       save(data)
       {
         this.loginservice.saveUser(data);
       }
  goToRegister()
  {
    this.router.navigate(['register']);
  }
  
}
  


