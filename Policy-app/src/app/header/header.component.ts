import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  uname:string;
  constructor() { }

  ngOnInit() {
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem('userid')
    this.uname=user;
    return !(user === null)
  }
}
