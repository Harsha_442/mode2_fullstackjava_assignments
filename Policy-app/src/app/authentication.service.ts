import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  uname:string;
  constructor(private http:HttpClient) { }

  private baseUrl ='http://localhost:8082/api/loginuser';



saveUser(data)
{
   console.log(data);
   console.log(data.lastName);
   sessionStorage.setItem('userid',data.lastName)
}

isUserLoggedIn() {
  let user = sessionStorage.getItem('userid')
  this.uname=user;
  return !(user === null)
}

loginUser(user:any) : Observable<any>
{
  console.log(user);
  const urlstr=this.baseUrl​​​​​​​​+"/"+​​​​​user;
  return this.http.post(this.baseUrl,user);

}

logout()
{
  sessionStorage.removeItem('userid');
}
}